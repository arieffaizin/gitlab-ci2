FROM golang:latest

# RUN add-apt-repository
ENTRYPOINT /go/src/github.com/purwandi/gin-app
WORKDIR /go/src/github.com/purwandi/gin-app

COPY . /go/src/github.com/purwandi/gin-app

RUN go get github.com/gin-gonic/gin
RUN cd /go/src/github.com/purwandi/gin-app && echo $PWD && ls -all  && make linux-amd64 && ls -all
RUN go run main.go
# RUN cd /opt/go/src/gin-app && echo $PWD && ls -all && 

EXPOSE 8080
